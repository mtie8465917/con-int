# Contenedor Nginx desde Dockerfile
FROM nginx:latest
WORKDIR /usr/share/nginx/html/
COPY . /usr/share/nginx/html/
VOLUME /var/log/nginx	
EXPOSE 80 443
CMD nginx -g 'daemon off;'